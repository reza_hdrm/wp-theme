<?php
include 'constant.php';
include 'app/autoloader.php';

add_action('after_setup_theme', 'Initializer::setup');
add_filter('show_admin_bar', '__return_false');
add_action('init', 'PostType::make_product_post_type');
add_action('init', 'Initializer::start_session');
add_action('add_to_cart', 'Basket::add');
add_action('init', function () {
    if (isset($_GET['remove_cart_item']))
        Basket::remove($_GET['remove_cart_item']);
});

add_action('add_meta_boxes', 'MetaBoxes::register_product_price_meta_box');
add_action('save_post', 'MetaBoxes::save_product_price');
add_action('add_meta_boxes', 'SliderMetaBox::register_product_slider_meta_box');
add_action('save_post', 'SliderMetaBox::save_product_slider');

add_filter('manage_product_posts_columns', 'PostType::add_price_columns');
add_action('manage_product_posts_custom_column', 'PostType::show_price_value_column', 10, 2);

add_shortcode('member_content', 'ShortCodes::member_content_handler');

//Cache::set('home_page_slider_query',array(1,2,3,4,5,6));