<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('.add_slider_item').on('click', function (event) {
            var wrapper = $('.sliders_item_wrapper');
            var item = '<p>\n' +
                '        <input style="width: 60%;direction: ltr" type="text" name="sliders[]">\n' +
                '        <a class="remove_slider_item" href="#"><span class="dashicon dashicons-dismiss"></span></a>\n' +
                '    </p>';
            event.preventDefault();
            wrapper.append(item);
        });
        $(document).on('click', '.remove_slider_item', function (event) {
            var $this = $(this);
            $this.parent().slideUp();
            event.preventDefault();
        });
    })
</script>
<p>
    <a href="#" class="add_slider_item">اضافه کردن آیتم</a>
</p>
<div class="sliders_item_wrapper">
    <?php if (is_array($slider_images) and count($slider_images) > 0): ?>
        <?php foreach ($slider_images as $slider_image): ?>
            <p>
                <input style="width: 60%;direction: ltr" type="text" name="sliders[]"
                       value="<?php echo $slider_image ?>">
                <a class="remove_slider_item" href="#"><span class="dashicon dashicons-dismiss"></span></a>
            </p>
        <?php endforeach; ?>
    <?php else: ?>
        <p>
            <input style="width: 60%;direction: ltr" type="text" name="sliders[]">
            <a class="remove_slider_item" href="#"><span class="dashicon dashicons-dismiss"></span></a>
        </p>
    <?php endif; ?>
</div>