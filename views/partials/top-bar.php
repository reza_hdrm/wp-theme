<!-- Top Bar
		============================================= -->
<?php
if (isset($_POST['add_to_cart'])) {
    $product_id = (int)$_POST['product_id'];
    $count = (int)$_POST['quantity'];
    if ($product_id > 0 and $count > 0)
        do_action('add_to_cart', $product_id, $count);
}
?>

<div id="top-bar">

    <div class="container clearfix">

        <div class="col_half nobottommargin hidden-xs">

            <p class="nobottommargin"><strong>Call:</strong> 1800-547-2145 | <strong>Email:</strong> info@canvas.com</p>

        </div>

        <div class="col_half col_last fright nobottommargin">

            <!-- Top Links
            ============================================= -->
            <div class="top-links">
                <ul>
                    <li><a href="#">USD</a>
                        <ul>
                            <li><a href="#">EUR</a></li>
                            <li><a href="#">AUD</a></li>
                            <li><a href="#">GBP</a></li>
                        </ul>
                    </li>
                    <li><a href="#">EN</a>
                        <ul>
                            <li><a href="#"><img src="<?php Asset::image('icons/flags/french.png') ?>" alt="French"> FR</a>
                            </li>
                            <li><a href="#"><img src="<?php Asset::image('icons/flags/italian.png') ?>" alt="Italian">
                                    IT</a></li>
                            <li><a href="#"><img src="<?php Asset::image('icons/flags/german.png') ?>" alt="German"> DE</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">ورود</a>
                        <div class="top-link-section">
                            <form id="top-login" role="form">
                                <div class="input-group" id="top-login-username">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="email" class="form-control" placeholder="کلمه عبور" required="">
                                </div>
                                <div class="input-group" id="top-login-password">
                                    <span class="input-group-addon"><i class="icon-key"></i></span>
                                    <input type="password" class="form-control" placeholder="کلمه عبور" required="">
                                </div>
                                <label class="checkbox">
                                    <input type="checkbox" value="remember-me"> مرا به خاطر بسپار
                                </label>
                                <button class="btn btn-danger btn-block" type="submit">ورود</button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div><!-- .top-links end -->

        </div>

    </div>

</div><!-- #top-bar end -->