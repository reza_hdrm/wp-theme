<!DOCTYPE html>
<html dir="rtl" lang="en-US">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="SemiColonWeb"/>

    <!-- Stylesheets
    ============================================= -->
    <!--<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />-->
    <link rel="stylesheet" href="<?php Asset::css('fontiran.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('bootstrap.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('bootstrap-rtl.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('style.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('style-rtl.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('dark.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('dark-rtl.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('font-icons.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('font-icons-rtl.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('animate.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('magnific-popup.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('responsive.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('responsive-rtl.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php Asset::css('fonts.css') ?>" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php wp_head() ?> <!-- Document Title
    ============================================= -->
    <!--<title>Home - Shop Layout 2 | Canvas</title>-->

</head>

<body class="stretched rtl">