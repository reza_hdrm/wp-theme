<?php


final class Autoloader
{

    /**
     * autoloader constructor.
     */
    private function __construct() {
        spl_autoload_register('self::autoload');
    }

    public static function getInstants() {
        new self();
    }
    private static function autoload($class_name) {
        $file = self::convert_class_to_file($class_name);
        if (is_file($file) and file_exists($file) and is_readable($file))
            include $file;
    }

    private static function convert_class_to_file($class_name): string {
        $class = strtolower($class_name);
        $class = 'class-' . $class;
        $file_name = $class . '.php';
        return THEME_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . $file_name;
    }
}

Autoloader::getInstants();