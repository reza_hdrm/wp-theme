<?php


class View
{
    public static function render(string $file_path) {
        $file_path = self::srtConvert($file_path);
        get_template_part('views' . DIRECTORY_SEPARATOR . $file_path);
    }

    public static function renderFile(string $file_path, $data = null) {
        $view_file_path = THEME_VIEW . DIRECTORY_SEPARATOR . self::srtConvert($file_path) . '.php';
        if (file_exists($view_file_path) and is_file($view_file_path) and is_readable($view_file_path)) {
            !empty($data) ? extract($data) : null;
            include $view_file_path;
        }
    }

    private static function srtConvert(string $file_path): string {
        $file_path = preg_replace('/[.]/', DIRECTORY_SEPARATOR, $file_path);
        return $file_path;
    }
}