<?php


class Asset
{
    public static function __callStatic($name, $arguments) {
        /*
         برای اضافه کردن هر متود static به کلاس باید نام آن را به آرایه names اضافه کرد و متناظر با آن دایرکتوری در فایل assets باشد
         */
        $names = ['css', 'js', 'image'];
        if (in_array($name, $names, true)) {
            $file_url = THEME_URL . '/assets/' . $name . '/' . $arguments[0];
            echo $file_url;
        }
    }
}