<?php


class ShortCodes
{
    public static function member_content_handler($atts, $content = '') {
        $options = OptionsPanel::load();
        if (isset($options['member_content_active']) and intval($options['member_content_active']) == 0)
            return $content;
        $args = shortcode_atts(array(
            'id' => 1,
            'role' => 'subscriber'
        ), $atts);
        //do_shortcode();
        if (!empty($content))
            if (is_user_logged_in())
                return $content;
            else
                return '<div class="member_only_contents"><p>این محتوا برای کاربران عضو شده در سایت دردسترس میباشد.</p></div>';
    }
}
//04:92:26:aa:8c:e7