<?php


class Utility
{
    public static function persian_number($data) {
        $fa_numbers = ['۰', '١', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $en_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        return str_replace($en_numbers, $fa_numbers, $data);
    }
}