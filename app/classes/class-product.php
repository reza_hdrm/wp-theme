<?php


class Product
{
    const  PRICE_META_KEY = 'product_price';
    const  PRICE_SALE_META_KEY = 'product_price_sale';

    public static function get_price(int $product_id, bool $in_persian = false) {
        if (!$product_id)
            return 0;
        $price = get_post_meta($product_id, self::PRICE_META_KEY, true);
        if (intval($price) > 0)
            if ($in_persian)
                return Utility::persian_number(number_format(apply_filters(self::PRICE_META_KEY, $price)));
            else
                return apply_filters(self::PRICE_META_KEY, $price);

        else
            return 0;
    }

    public static function get_price_sale(int $product_id, bool $in_persian = false) {
        if (!$product_id)
            return 0;
        $price_sale = get_post_meta($product_id, self::PRICE_SALE_META_KEY, true);
        if (intval($price_sale) > 0)
            if ($in_persian)
                return Utility::persian_number(number_format(apply_filters(self::PRICE_SALE_META_KEY, $price_sale)));
            else
                return apply_filters(self::PRICE_SALE_META_KEY, $price_sale);
        else
            return 0;
    }

    public static function get_slider_images(int $product_id) {
        $slider_images = get_post_meta($product_id, 'slider_images', true);
        if (!empty($slider_images) and is_array($slider_images) and count($slider_images) > 0)
            return $slider_images;
        return 0;
    }

    public static function find(int $product_id) {
        $product = get_post($product_id);
        return $product;
    }
}