<?php


class Basket
{
    public static function add(int $product_id, int $count = 1) {
        self::init_basket();
        if (self::exist($product_id))
            $_SESSION['basket']['items'][$product_id]['count']++;
        else {
            $product = Product::find($product_id);
            $_SESSION['basket']['items'][$product_id] = [
                'title' => $product->post_title,
                'price' => Product::get_price($product_id),
                'count' => $count
            ];
        }
    }

    public static function remove(int $product_id) {
        if (self::exist($product_id))
            unset($_SESSION['basket']['items'][$product_id]);
    }

    public static function update(int $product_id, int $count) {
        if (self::exist($product_id)) {
            if ($count == 0)
                return self::remove($product_id);
            $_SESSION['basket']['items'][$product_id]['count'] = $count;
        }
    }

    public static function exist(int $product_id): bool {
        return isset($_SESSION['basket']['items'][$product_id]);
    }

    public static function total_count(): int {
        if (isset($_SESSION['basket']['items']) and count($_SESSION['basket']['items']) > 0)
            return count($_SESSION['basket']['items']);
        return 0;
    }

    public static function items(): array {
        if (isset($_SESSION['basket']['items']) and count($_SESSION['basket']['items']) > 0)
            return $_SESSION['basket']['items'];
        return [];
    }

    public static function total_price(): int {
        if (isset($_SESSION['basket']['items']))
            return array_reduce($_SESSION['basket']['items'], function ($total, $item) {
                $total += $item['count'] * $item['price'];
                return $total;
            }, 0);
        return 0;
    }

    public static function subtotal(array $item): int {
        return $item['price'] * $item['count'];
    }

    public static function init_basket() {
        if (!isset($_SESSION['basket']))
            $_SESSION['basket'];
    }
}