<?php


class OptionsPanel
{

    /**
     * OptionsPanel constructor.
     */
    public function __construct() {
        $this->init_panel();
        add_action('hdrm_save_option_panel', array($this, 'save_option_handler'));
    }

    public function init_panel(): void {
        add_action('admin_menu', array($this, 'add_panel_menu'));
    }

    public function add_panel_menu(): void {
        add_theme_page(
            'تنظیمات قالب',
            'تنظیمات قالب',
            'manage_options',
            'hdrm_options_panel',
            array($this, 'panel_page_content')
        );
    }

    public function panel_page_content(): void {
        if (isset($_POST['save_options']))
            do_action('hdrm_save_option_panel');
        $options = self::load();
        View::renderFile('admin.options_panel.index', compact('options'));
    }

    public function save_option_handler() {
        $options = self::load();
        $options['member_content_active'] = isset($_POST['member_content_active']) ? 1 : 0;
        self::update($options);
    }

    public static function load() {
        return get_option(THEME_OPTIONS);
    }

    public static function update(array $options): bool {
        return update_option(THEME_OPTIONS, $options);
    }
}