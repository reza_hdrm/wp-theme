<?php


class Cache
{
    public static function get($key) {
        return get_transient($key);
    }

    public static function set($key, $value, int $expire = HOUR_IN_SECONDS): bool {
        return set_transient($key, $value, $expire);
    }

    public static function delete($key): bool {
        return delete_transient($key);
    }
}