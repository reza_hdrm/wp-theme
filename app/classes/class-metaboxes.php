<?php


class MetaBoxes
{
    public static function register_product_price_meta_box() {
        add_meta_box(
            'product_price_meta_box',
            'قیمت محصول',
            'MetaBoxes::product_price_content_handler',
            'product'
        );
    }

    public static function product_price_content_handler($post) {
        $product_price = (int)get_post_meta($post->ID, 'product_price', true);
        $product_price_sale = (int)get_post_meta($post->ID, 'product_price_sale', true);
        View::renderFile('admin.metabox.product.product_price', array(
            'product_price' => $product_price,
            'product_price_sale' => $product_price_sale,
        ));
    }

    public static function save_product_price($post_id) {
        //بعد از شرایط
        if (isset($_POST[Product::PRICE_META_KEY]) and (int)$_POST[Product::PRICE_META_KEY] > 0)
            update_post_meta($post_id, 'product_price', $_POST['product_price']);
        if (isset($_POST[Product::PRICE_SALE_META_KEY]) and (int)$_POST[Product::PRICE_SALE_META_KEY] > 0)
            update_post_meta($post_id, 'product_price_sale', $_POST['product_price_sale']);
    }
}